FROM openjdk:11-jre-slim-buster
EXPOSE 8080
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
ENTRYPOINT ["jar","-jar","/app.jar"]